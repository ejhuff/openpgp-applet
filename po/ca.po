# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Tails developers
# This file is distributed under the same license as the OpenPGP_Applet package.
#
# Translators:
# laia_, 2016
msgid ""
msgstr ""
"Project-Id-Version: The Tor Project\n"
"Report-Msgid-Bugs-To: tails@boum.org\n"
"POT-Creation-Date: 2017-08-05 15:07-0400\n"
"PO-Revision-Date: 2018-02-20 19:11+0000\n"
"Last-Translator: Adolfo Jayme-Barrientos\n"
"Language-Team: Catalan (http://www.transifex.com/otf/torproject/language/"
"ca/)\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: bin/openpgp-applet:160
msgid "You are about to exit OpenPGP Applet. Are you sure?"
msgstr ""
"Esteu a punt de sortir de la miniaplicació de l'OpenPGP. N'esteu segurs? "

#: bin/openpgp-applet:172
msgid "OpenPGP encryption applet"
msgstr "Miniaplicació de xifrat OpenPGP"

#: bin/openpgp-applet:175
msgid "Exit"
msgstr "Surt"

#: bin/openpgp-applet:177
msgid "About"
msgstr "Quant a"

#: bin/openpgp-applet:232
msgid "Encrypt Clipboard with _Passphrase"
msgstr "Xifra el Portapapers amb la _Contrasenya"

#: bin/openpgp-applet:235
msgid "Sign/Encrypt Clipboard with Public _Keys"
msgstr "Signa/Xifra el Portapapers amb les _Claus Públiques"

#: bin/openpgp-applet:240
msgid "_Decrypt/Verify Clipboard"
msgstr "_Desxifra/Verifica el Portapapers"

#: bin/openpgp-applet:244
msgid "_Manage Keys"
msgstr "_Gestiona les Claus"

#: bin/openpgp-applet:248
msgid "_Open Text Editor"
msgstr "_Obre l'editor de textos"

#: bin/openpgp-applet:292
msgid "The clipboard does not contain valid input data."
msgstr "El portapapers no conté dades d'entrada vàlides."

#: bin/openpgp-applet:337 bin/openpgp-applet:339 bin/openpgp-applet:341
msgid "Unknown Trust"
msgstr "Confiança desconeguda"

#: bin/openpgp-applet:343
msgid "Marginal Trust"
msgstr "Confiança marginal"

#: bin/openpgp-applet:345
msgid "Full Trust"
msgstr "Plena confiança"

#: bin/openpgp-applet:347
msgid "Ultimate Trust"
msgstr "Confiança fins al final"

#: bin/openpgp-applet:400
msgid "Name"
msgstr "Nom"

#: bin/openpgp-applet:401
msgid "Key ID"
msgstr "ID de la Clau"

#: bin/openpgp-applet:402
msgid "Status"
msgstr "Estat"

#: bin/openpgp-applet:433
msgid "Fingerprint:"
msgstr "Empremta:"

#: bin/openpgp-applet:436
msgid "User ID:"
msgid_plural "User IDs:"
msgstr[0] "ID d'usuari:"
msgstr[1] "IDs d'usuari:"

#: bin/openpgp-applet:465
msgid "None (Don't sign)"
msgstr "Cap (no signat)"

#: bin/openpgp-applet:528
msgid "Select recipients:"
msgstr "Seleccioneu els destinataris"

#: bin/openpgp-applet:536
msgid "Hide recipients"
msgstr "Oculta els destinataris"

#: bin/openpgp-applet:539
msgid ""
"Hide the user IDs of all recipients of an encrypted message. Otherwise "
"anyone that sees the encrypted message can see who the recipients are."
msgstr ""
"Oculta els IDs d'usuari de tots els destinataris del missatge xifrat. "
"Altrament, qualsevol que vegi el missatge xifrat veurà qui són els "
"destinataris."

#: bin/openpgp-applet:545
msgid "Sign message as:"
msgstr "Signa el missatge com:"

#: bin/openpgp-applet:549
msgid "Choose keys"
msgstr "Tria les claus"

#: bin/openpgp-applet:589
msgid "Do you trust these keys?"
msgstr "Confieu en aquestes claus?"

#: bin/openpgp-applet:592
msgid "The following selected key is not fully trusted:"
msgid_plural "The following selected keys are not fully trusted:"
msgstr[0] "La clau sel·leccionada no és de plena confiança:"
msgstr[1] "Les claus seleccionades no són de plena confiança:"

#: bin/openpgp-applet:610
msgid "Do you trust this key enough to use it anyway?"
msgid_plural "Do you trust these keys enough to use them anyway?"
msgstr[0] "Confieu prou en aquesta clau per usar-la igualment? "
msgstr[1] "Confieu prou en aquestes claus per usar-les igualment?"

#: bin/openpgp-applet:623
msgid "No keys selected"
msgstr "No hi ha cap clau seleccionada"

#: bin/openpgp-applet:625
msgid ""
"You must select a private key to sign the message, or some public keys to "
"encrypt the message, or both."
msgstr ""
"Heu de seleccionar una clau privada per signar el missatge o alguna clau "
"pública per xifrar el missatge, o les dues."

#: bin/openpgp-applet:653
msgid "No keys available"
msgstr "No hi ha claus disponibles"

#: bin/openpgp-applet:655
msgid ""
"You need a private key to sign messages or a public key to encrypt messages."
msgstr ""
"Necessiteu una clau privada per signar missatges o una clau pública per "
"xifrar missatges."

#: bin/openpgp-applet:783
msgid "GnuPG error"
msgstr "Hi ha hagut un error de GnuPG"

#: bin/openpgp-applet:804
msgid "Therefore the operation cannot be performed."
msgstr "Per tant, l'operació no es pot realitzar."

#: bin/openpgp-applet:854
msgid "GnuPG results"
msgstr "Resultats del GnuPG"

#: bin/openpgp-applet:860
msgid "Output of GnuPG:"
msgstr "Sortida del GnuPG:"

#: bin/openpgp-applet:885
msgid "Other messages provided by GnuPG:"
msgstr "Altres missatges entregats per GnuPG:"
